Learning AWS is a journey. Here I document mine in hopes it may help anyone who finds it, while organizing and keeping historical progress of what I have already learned.


## AWS - Basics

### Networking

[Learning the VPC Visualized](https://start.jcolemorrison.com/aws-vpc-core-concepts-analogy-guide/#overview)

`VIDEO` [AWS VPC & Subnets by Max](https://youtu.be/bGDMeD6kOz0)

`VIDEO` [IPv4, CIDIR, VPCs Made Simple by The Simple Engineer](https://youtu.be/z07HTSzzp3o)

`LINUX ACADEMY HANDS-ON` [Create And Configure Basic VPC Components in AWS](https://linuxacademy.com/hands-on-lab/344672a2-6d75-449f-98c3-6d682b481565/)



## AWS - Intermediate

### Networking

`LINUX ACADEMY HANDS-ON` [ Building a Three-Tier Network VPC from Scratch in AWS ](https://linuxacademy.com/hands-on-lab/f2a24706-8b7b-4a21-9ad6-859bd5595215/)

`LINUX ACADEMY HANDS-ON` [Create a VPC Endpoint and S3 bucket in AWS](https://linuxacademy.com/hands-on-lab/37331c72-e3f1-4ded-9607-61d993fbb5a5/)


### Serverless

`LINUX ACADEMY HANDS-ON` [Creating a Simple AWS Lambda Function](https://linuxacademy.com/hands-on-lab/f2b58b6b-2a05-435a-8746-ca1ff25b9773/)

`LINUX ACADEMY HANDS-ON` [ Deploying a Serverless Application Using AWS SAM ](linuxacademy.com/hands-on-labs/b79c1044-d211-471c-8447-42c7958b2908)


## Infrastructure as Code - Basics

### Terraform

`LINUX ACADEMY HANDS-ON` [Working with Terraform Variables](https://linuxacademy.com/hands-on-lab/340963f9-b07f-4cb9-b3b1-f7f225eb815e/)

### Cloudformation

`LINUX ACADEMY HANDS-ON` [Deploy a basic infrastructure with Cfn templates](linuxacademy.com/hands-on-labs/c0ad833c-b48f-4072-8c9a-13f0358e0666)



## Infrastructure as Code - Intermediate

### Cloudformation

`LINUX ACADEMY HANDS-ON` [Understanding CloudFormation Template Anatomy ](linuxacademy.com/hands-on-labs/029ff27d-55cd-4491-ae54-155188196105)